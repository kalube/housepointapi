from flask import request

from apikeys import apikeys

def authenticate(f):
    def wrapper(*args, **kw):
        if not 'key' in request.args:
            return {
                "message": "You must provide an API key"
            }
        else:
            key = request.args['key']
            #print("Key was:", apikeys[[k['key'] for k in apikeys].index(key)]['description'])
            #print(f)
            return f()
    return wrapper