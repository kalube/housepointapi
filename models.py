

class House():

    def __init__(self, houseId: int, HouseName: str, HousePoints: int = 0):
        self.houseId = houseId
        self.houseId = HouseName
        self.housePoints = HousePoints
    
    def __init__(self, dbRow : tuple):
        self.houseId = int(dbRow[0])
        self.houseName = dbRow[1]
        self.housePoints = int(dbRow[2])
    

    def __eq__(self, shorthand):
        if type(shorthand) is int:
            return self.houseId == shorthand
        if type(shorthand) is str and len(shorthand) == 1:
            return self.houseName[0].lower() == shorthand.lower()
        
        return self.houseName.lower() == shorthand.lower()
    
    def __str__(self):
        return "House ID: {}\nHouse Name: {}".format(self.houseId, self.houseId)
    
    def serialize(self):
        return self.__dict__

class User():
    def __init__(self, userId: int, discordId: int, houseId: int, memberRolesId: int, housePoints: int):
        self.userId = userId
        self.discordId = discordId
        self.houseId = houseId
        self.memberRolesId = memberRolesId
        self.housePoints = housePoints
    
    def __init__(self, dbRow: tuple):
        self.userId = int(dbRow[0])
        self.discordId = int(dbRow[1] if dbRow[1] is not None else 0)
        self.houseId = int(dbRow[2] if dbRow[2] is not None else 0)
        self.memberRolesId = int(dbRow[3] if dbRow[3] is not None else 0)
        self.housePoints = int(dbRow[4] if dbRow[4] is not None else 0)
    