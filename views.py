from run import app
from flask import jsonify


@app.route('/api/v1')
def index():
    return jsonify({'message': 'Hello, World!'})