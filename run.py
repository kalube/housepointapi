from flask import Flask, request
from flask_restful import Api
from flask_injector import FlaskInjector

from database import HouseDbContext

app = Flask(__name__)
api = Api(app)

API_ROOT = "/api/v1"

import views, models, resources, database

api.add_resource(resources.GivePoints, f'{API_ROOT}/givePoints')
api.add_resource(resources.GetPoints, f'{API_ROOT}/getPoints')

def configure(binder):
    binder.bind(
        HouseDbContext,
        to=HouseDbContext("data/data.db"),
        scope=request
    )

# Initialize Flask-Injector. This needs to be run *after* you attached all
# views, handlers, context processors and template globals.

FlaskInjector(app=app, modules=[configure])