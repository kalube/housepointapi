import sqlite3

from models import House, User

# Database abstraction layer, simplifies db lookups
class HouseDbContext():
    def __init__(self, dbfile):
        self._file = dbfile
        self.conn = None
        self.cursor = None
    
    def connect(self):
        self.conn = sqlite3.connect(self._file)
        self.cursor = self.conn.cursor()
    
    def get_houses(self) -> list:
        return [House(x) for x in self.cursor.execute("SELECT * FROM Houses").fetchall()]
    
    def get_house(self, houseId: int):
        return House(self.cursor.execute("SELECT * FROM Houses WHERE HouseId={}".format(houseId)).fetchall()[0])

    def update_housepoints(self, house: House) -> bool:
        self.cursor.execute("UPDATE Houses SET HousePoints=? WHERE HouseId=?", (house.housePoints, house.houseId))
        self.conn.commit()
    

    def get_user(self, userId: int) -> User:
        return User(self.cursor.execute("SELECT * FROM Users WHERE UserId={}".format(userId)).fetchall()[0])
    
    def set_discordId(self, userId: int, discordId: int) -> bool:
        ret = self.cursor.execute("UPDATE Users SET DiscordId=? WHERE UserId=?", (discordId, userId))
        self.conn.commit()
        return ret