from flask_restful import Resource, reqparse
from flask import request
from flask_injector import FlaskInjector
from injector import inject

from database import HouseDbContext

from auth import authenticate
from models import *

# class DbContext(Module):
#     @provider
#     @singleton
#     def provide_ext(self, app: Flask) -> HouseDbContext:
#         return HouseDbContext("data/data.db")
class GivePoints(Resource):
    method_decorators = [authenticate]

    @inject
    def __init__(self, *args, dbContext: HouseDbContext, **kwargs):
        self._ctx = dbContext
        self._ctx.connect()
        super().__init__(*args, **kwargs)

    def post(self):
        if not 'user' in request.form:
            return {
                "message": "Must specify a user to receive points"
            }
        userid = request.form['user']
        user = self._ctx.get_user(userid)
        return user.__dict__

class GetPoints(Resource):
    @inject
    def __init__(self, *args, dbContext: HouseDbContext, **kwargs):
        self._ctx = dbContext
        self._ctx.connect()
        super().__init__(*args, **kwargs)

    def get(self):
        print(request.args)
        if not ('user' in request.args or 'house' in request.args):
            return {
                "message": "Must specify a house or user to get points"
            }
        if 'user' in request.args:
            user = self._ctx.get_user(request.args['user'])
            return {
                "userId": request.args['user'],
                "discordId": user.discordId,
                "housePoints": user.housePoints
            }
        return request.args

class UpdateUser(Resource):
    @inject
    def __init__(self, *args, dbContext: HouseDbContext, **kwargs):
        self._ctx = dbContext
        self._ctx.connect()
        super().__init__(*args, **kwargs)
    
    def post(self):
        if 'discordId' in request.form:
            self._ctx.set_discordId(request.form['user'], request.form['discordId'])